const express = require('express');
const app = express();
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const adapter = new FileSync('../Database/database.json');
const db = low(adapter);
const cors = require('cors');
const bodyParser = require('body-parser');
const shortid = require('shortid');
const socketIo = require('socket.io');
app.use(cors({ credentials: true, origin: 'http://localhost:8080' }));
app.use(bodyParser.urlencoded({
    extended: true
  }));
db.defaults({ referees: [], classes: [], horses: [] }).write();
const server = require('http').createServer(app);

//Referees
app.post('/referee', (req, res) => {
    db.get('referees')
        .push({
            id: shortid.generate(),
            sedzia: req.body.sedzia,
            kraj: req.body.kraj
        })
        .write();
    res.status(201).send("Referee has been created");
});

app.get('/referee', (req, res) => {
    const referees = db.get('referees');
    res.json(referees);
});

app.get('/referee/:id', (req, res) => {
    const id = req.params.id;
    const referee = db.get('referees').find({ id: id }).value();
    res.json(referee);
});

app.delete('/referee/:id', (req, res) => {
    const id = req.params.id;
    db.get('referees')
        .remove({ id: id })
        .write();
    res.status(200).send("Referee has been removed");
});

app.put('/referee/:id', (req, res) => {
    const id = req.params.id;
    console.log(req.body.sedzia);
    console.log(req.body.kraj);
    db.get('referees')
        .find({ id: id })
        .assign({
            sedzia: req.body.sedzia,
            kraj: req.body.kraj
        }).write();

    res.status(200).send("Referee has been updated");
});

/* Classes */

app.post('/class', (req, res) => {
    db.get('classes')
        .push({
            id: shortid.generate(),
            number: req.body.number,
            category: req.body.category,
            comission: JSON.parse(req.body.comission)
        })
        .write();

    res.status(201).send("Class has been created");
});

app.get('/class', (req, res) => {
    const classes = db.get('classes');
    res.json(classes);
});

app.get('/class/:id', (req, res) => {
    const id = req.params.id;
    const singleClass = db.get('classes').find({ id: id }).value();

    res.json(singleClass);
});

app.delete('/class/:id', (req, res) => {
    const id = req.params.id;
    db.get('classes')
        .remove({ id: id })
        .write();
    res.status(200).send("Class has been removed");
});

app.put('/class/:id', (req, res) => {
    const id = req.params.id;
    db.get('classes')
        .find({ id: id })
        .assign({
            numer: req.body.number,
            kategoria: req.body.category,
            comission: JSON.parse(req.body.comission)
        })
        .write();

    res.status(200).send("Class has been updated");
});

/* Horsers */

app.post('/horse', (req, res) => {
    db.get('horses')
        .push(JSON.parse(req.horse))
        .write();
    res.status(201).send("Referee has been created");
});

app.get('/horse', (req, res) => {
    const horses = db.get('horses');

    res.json(horses);
});

app.get('/horse/:id', (req, res) => {
    const horse = db.get('horses').find({ id: req.body.horse.id }).value();

    res.json(horse);
});

app.delete('/horse/:id', (req, res) => {
    const id = req.params.id;
    db.get('horses').remove({ id: id }).write();
    res.status(200).send("Horse has been removed on id = " + id);
});

app.put('/horse/:id', (req, res) => {
    let simpleHorse = JSON.parse(req.body.horse);
    db.get('horses')
        .find({ id: simpleHorse.id })
        .assign(simpleHorse)
        .write();
    res.status(200).send("Horse has been updated");
});

//port servera
server.listen(3001, () => {
    console.log('Serwer pod adresem http://localhost:3001/');
});
/* eslint-disable no-console */
import Vue from "vue";
import axios from "axios";
import Vuex from "vuex";


Vue.use(Vuex);

const base = axios.create({
    baseURL: "http://localhost:3001/",
    withCredentials: true
});
Vue.prototype.$http = base;

Vue.config.productionTip = false;

const store = new Vuex.Store({
    state: {
        referees: [],
        classes: [],
        horses: []
    },
    actions: {
        loadReferees({
            commit
        }) {
            base.get("referee").then((response) => {
                commit("setReferees", response.data);
            })
                .catch((errors) => {
                    console.log(errors);
                });
        },
        loadClasses({
            commit
        }) {
            base.get("class").then((response) => {
                commit("setClasses", response.data);
            })
                .catch((errors) => {
                    console.log(errors);
                });
        },
        loadHorses({
            commit
        }) {
            base.get("horse").then((response) => {
                commit("setHorses", response.data);
            })
                .catch((errors) => {
                    console.log(errors);
                });
        },
    },
    mutations: {
        setReferees(state, referees) {
            state.referees = referees;
        },
        setClasses(state, classes) {
            state.classes = classes;
        },
        setHorses(state, horses) {
            state.horses = horses;
        }
    }
});

export default store;

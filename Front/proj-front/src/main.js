import Vue from 'vue'
import App from './App.vue'
import 'bootstrap'; 
import 'bootstrap/dist/css/bootstrap.min.css';
import VueRouter from 'vue-router';
import HorseCrud from './components/HorseCrud.vue';
import ClassCrud from './components/ClassCrud.vue';
import RefereeCrud from './components/RefereeCrud.vue';
import VueSocketIO from "vue-socket.io";
import axios from "axios";
import store from "./store";

Vue.use(VueRouter);

const routes = [
  { path: '/horses', component: HorseCrud },
  { path: '/referee', component: RefereeCrud },
  { path: '/classes', component: ClassCrud }

];

const router = new VueRouter({
  routes // short for `routes: routes`
})

Vue.use(new VueSocketIO({
  debug: true,
  connection: "http://localhost:3001",
}));


const base = axios.create({
  baseURL: "http://localhost:3001/",
  withCredentials: true
});
Vue.prototype.$http = base;

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
